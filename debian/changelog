python-feather-format (0.3.1+dfsg1-8) UNRELEASED; urgency=medium

  * Team upload.
  * Verify new upstream versions >0.3.1 are wrapper around pyarrow and thus
    can not be packaged before pyarrow is packaged for Debian.
  * Standards-Version: 4.6.2
  * Build-Depends: s/dh-python/dh-sequence-python3/
  * Rules-Requires-Root: no
  * Watch file standard 4
  * Adapt to Pandas 2.0
    Closes: #1044071

 -- Andreas Tille <tille@debian.org>  Sun, 04 Feb 2024 07:21:36 +0100

python-feather-format (0.3.1+dfsg1-7) unstable; urgency=medium

  * Team upload

  [ Bas Couwenberg ]
  * Switch to cython3-legacy (Closes: #1056856)

  [ Graham Inggs ]
  * Drop the runtime dependency on Cython (Closes: #1058001)
  * Add test dependency on python3-setuptools to avoid
    autopkgtest failure with Python 3.12

 -- Graham Inggs <ginggs@debian.org>  Thu, 14 Dec 2023 13:55:22 +0000

python-feather-format (0.3.1+dfsg1-6) unstable; urgency=medium

  * Team upload.
  * Patch: Support setuptools 60. Closes: #1022475

 -- Stefano Rivera <stefanor@debian.org>  Sat, 05 Nov 2022 17:27:42 +0200

python-feather-format (0.3.1+dfsg1-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 11:57:25 -0400

python-feather-format (0.3.1+dfsg1-4) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Drew Parsons ]
  * debian patch test_pandas.patch enables build against pandas 1.0.
    Thanks Rebecca Palmer. Closes: #950924.

 -- Drew Parsons <dparsons@debian.org>  Mon, 24 Aug 2020 17:18:26 +0800

python-feather-format (0.3.1+dfsg1-3) unstable; urgency=medium

  * Team upload

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Rebecca N. Palmer ]
  * Be compatible with pandas 0.25 (Closes: #943925)

 -- Graham Inggs <ginggs@debian.org>  Fri, 22 Nov 2019 09:27:00 +0000

python-feather-format (0.3.1+dfsg1-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support (Closes: #841552, #904115).
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).
  * Add autopkgtests.
  * Add python3-six to {build-,}depends and python3-pandas to depends
    (Closes: #896370).
  * Enable all hardening.
  * Change B-D from python3-dev to python3-all-dev (Closes: #868971).

 -- Ondřej Nový <onovy@debian.org>  Thu, 08 Aug 2019 16:18:28 +0200

python-feather-format (0.3.1+dfsg1-1) unstable; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Fri, 25 Nov 2016 18:32:17 +0800

python-feather-format (0.3.0+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * Bump compat to 10.
  * Remove Multi-Arch: foreign.
  * Set LC_ALL=C.UTF-8 for test.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Thu, 13 Oct 2016 18:52:55 +0800

python-feather-format (0.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.8.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Thu, 05 May 2016 19:08:48 +0800

python-feather-format (0.1.2-1) unstable; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Mon, 11 Apr 2016 23:10:05 +0800

python-feather-format (0.1.1-1) unstable; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Tue, 05 Apr 2016 14:15:34 +0800

python-feather-format (0.1.0-1) unstable; urgency=low

  * Initial release. Closes: #819526

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Wed, 30 Mar 2016 19:01:30 +0800
